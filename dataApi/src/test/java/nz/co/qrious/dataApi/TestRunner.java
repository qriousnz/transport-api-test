package nz.co.qrious.dataapi;

import cucumber.api.java.Before;
import cucumber.api.junit.Cucumber;
import nz.co.qrious.sql.dbConnection;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.*;


/**
 * Created by jingbai on 7/28/17.
 */
@RunWith(Cucumber.class)
@Cucumber.Options (
        format ={"json:target/cucumber.json","json:../target/cucumber_dataApi.json"},
        features = "src/test/resource",
        glue = { "nz.co.qrious.dataapi"},
        tags = { "@dataApi"}
)
public class TestRunner {


    @BeforeClass
    public static void testDataPreparation() throws SQLException, IOException {
        System.out.println("Before test start, insert test data into test database");

        dbConnection conn = new dbConnection();
        conn.insertBatch();

    }

    @AfterClass
    public static void testDataCleanup() throws IOException, SQLException {

        System.out.println("After test is done, cleanup the test databases.");

        dbConnection conn = new dbConnection();
        conn.deleteBatch();

    }


}

