package nz.co.qrious.dataapi;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import gherkin.formatter.model.DataTableRow;
import org.junit.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.restassured.path.json.JsonPath.from;

/**
 * Created by jingbai on 7/28/17.
 */
public class timeDelaySteps extends BaseUtil {

    private BaseUtil base;

    public timeDelaySteps(BaseUtil base)
    {
        this.base = base;
    }

    private ArrayList<Map<String,Object>> jsonAsArrayList;


    @Then("^user should see \"([^\"]*)\" time delay data object in the response body$")
    public void user_should_see_time_delay_data_object_in_the_response_body(int number) throws Throwable {

        jsonAsArrayList = from(base.response.asString()).get("data");

        Assert.assertEquals(number, jsonAsArrayList.size());
    }

    @Then("^user should see items object with below pairs on the label \"([^\"]*)\" node$")
    public void user_should_see_items_object_with_below_pairs_on_the_label_node(String keyword, DataTable dataTable) throws Throwable {

        //put the value of "date" and "value" parameters of the items object in response body in to a List
        List<Integer> source = new ArrayList<Integer>();
        String path = "data.find { it.label == 'Christchurch Roundabout (" + keyword + ")' }.items";

        List<Map<String,Integer>> itemList = from(base.response.asString()).getList(path);

        for (Map<String,Integer> itemMap : itemList)
        {
            source.add(itemMap.get("date"));
            source.add(itemMap.get("value"));

            if (itemMap.size() == 3 )
                source.add(itemMap.get("compareValue"));
        }

//        for (int i = 0; i < source.size(); i++) {
//            System.out.println(source.get(i));
//        }


        //Translate the date and value of the dataTable into the same format as the response
        List<Integer> target = new ArrayList<Integer>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Pacific/Auckland"));

        for (DataTableRow row : dataTable.getGherkinRows()) {
            Date dateS = dateFormat.parse(row.getCells().get(0));
            target.add(new Long(dateS.getTime()/1000).intValue());  // Date -> Long(unixTime) -> Integer(unixTime)

            target.add(Integer.valueOf((row.getCells().get(1))));  // String -> Integer

            if (row.getCells().size() == 3 )
            {
                if (row.getCells().get(2).equals("null"))         // String "null" -> null
                    target.add(null);
                else
                    target.add(Integer.valueOf((row.getCells().get(2))));  // String -> Integer
            }
        }

        //Assert the response ( source list ) and the target list
        for (int i = 0; i < target.size(); i++) {
            Assert.assertEquals(target.get(i), source.get(i));
        }

    }





}
