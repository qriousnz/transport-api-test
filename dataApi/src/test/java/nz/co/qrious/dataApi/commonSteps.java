package nz.co.qrious.dataapi;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import nz.co.qrious.dataapi.BaseUtil;
import org.junit.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;

/**
 * Created by jingbai on 7/27/17.
 */
public class commonSteps extends BaseUtil {

    private BaseUtil base;

    public commonSteps (BaseUtil base)
    {
        this.base = base;
    }

    private String dkronUrl;
    //private String url;


    @Given("^the Dkron job base path is \"([^\"]*)\"$")
    public void the_Dkron_job_base_path_is(String url) throws Throwable {
        this.dkronUrl = url;
    }


    @When("^user configures the Dkron job name to \"([^\"]*)\"$")
    public void user_configures_the_Dkron_job_name_to(String path) throws Throwable {
        this.dkronUrl = this.dkronUrl + path;
        //System.out.println("Dkron job URL is " + this.dkronUrl);
    }

    @When("^perform the post request to trigger the job$")
    public void perform_the_post_request_to_trigger_the_job() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();

        base.response = given()
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .post(this.dkronUrl)
                .then()
                .log().all()
                .extract()
                .response();

        //System.out.println(base.response.asString());
    }

    @Then("^the response code should be (\\d+)$")
    public void the_response_code_should_be(int expectedCode) throws Throwable {
        Assert.assertEquals(expectedCode, base.response.getStatusCode());
    }

    @Given("^the API endpoint base path is \"([^\"]*)\"$")
    public void the_API_endpoint_base_path_is(String url) throws Throwable {
        base.apiUrl = url;
    }

    @When("^user setup a request path to \"([^\"]*)\"$")
    public void user_setup_a_request_path_to(String path) throws Throwable {
        base.apiUrl =  base.apiUrl + path;
    }

    @When("^perform the get request$")
    public void perform_the_get_request() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();

        //System.out.println("perform the get request URL is " + base.apiUrl);

        base.response = given()
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .get( base.apiUrl)
                .then()
                .log().all()
                .extract()
                .response();

        //System.out.println(base.response.asString());
    }

    @When("^set the \"([^\"]*)\" parameter to \"([^\"]*)\"$")
    public void set_the_parameter_to(String param, String valueInput) throws Throwable {

        String value = valueInput;

        //In some case, value may be date yyyy-MM-dd hh:mm:ss, need to translate to unix timestamp
        if((valueInput.contains("-")) && (valueInput.contains(":"))) {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("Pacific/Auckland"));

            Date dateS = dateFormat.parse(valueInput);
            value = String.valueOf(dateS.getTime()/1000);  // Date - > Long(unixTime) -> String(unixTime)

            //System.out.println("value is " + value);
        }


        if(base.apiUrl.contains("?") ) {
            base.apiUrl = base.apiUrl + "&" + param + "=" + value;
        }
        else {
            base.apiUrl = base.apiUrl + "?" + param + "=" + value;
        }
    }

    @When("^user specifies the time range from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void user_specifies_the_time_range_from_to(String dateStart, String dateEnd) throws Throwable {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Pacific/Auckland"));

        Date dateS = dateFormat.parse(dateStart);
        long unixTimeS = (long) dateS.getTime()/1000;

        Date dateE = dateFormat.parse(dateEnd);
        long unixTimeE = (long) dateE.getTime()/1000;

        base.apiUrl =  base.apiUrl + "?" + "starttime=" + unixTimeS + "&endtime=" + unixTimeE;
    }


    @Then("^wait for \"([^\"]*)\" seconds before test the other scenarios$")
    public void wait_for_seconds_before_test_the_other_scenarios(long sec) throws Throwable {
        TimeUnit.SECONDS.sleep(sec);
    }

}
