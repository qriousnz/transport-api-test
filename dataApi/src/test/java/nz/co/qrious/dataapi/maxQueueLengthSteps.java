package nz.co.qrious.dataapi;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.qrious.sql.dbConnection;
import org.junit.Assert;
import org.junit.BeforeClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.restassured.path.json.JsonPath.from;

public class maxQueueLengthSteps {

    private BaseUtil base;

    public maxQueueLengthSteps (BaseUtil base)
    {
        this.base = base;
    }

    private ArrayList<Map<String,Object>> jsonAsArrayList;



    @Then("^user should see \"([^\"]*)\" max queue length data object in the response body$")
    public void user_should_see_max_queue_length_data_object_in_the_response_body(int count) throws Throwable {

        jsonAsArrayList = from(base.response.asString()).get("data");

        System.out.println("jsonAsArrayList  is: " + jsonAsArrayList.toString());

        Assert.assertEquals(count, jsonAsArrayList.size());

    }

    @Then("^the value in the max data object is \"([^\"]*)\"$")
    public void the_value_in_the_max_data_object_is(int number) throws Throwable {
        List<Integer> valueList = from(base.response.asString()).getList("data.value");
        System.out.println("value List  is: " + valueList.toString());

        for (Integer value : valueList) {
            System.out.print(value.intValue());
            Assert.assertEquals(number,value.intValue());
        }

    }


}
