package nz.co.qrious.dataapi;

import cucumber.api.java.en.Then;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static io.restassured.path.json.JsonPath.from;

/**
 * Created by jingbai on 7/28/17.
 */
public class currentTimeDelaySteps extends BaseUtil {

    private BaseUtil base;

    public currentTimeDelaySteps(BaseUtil base)
    {
        this.base = base;
    }

    private ArrayList<Map<String,Object>> jsonAsArrayList;


    @Then("^user should see \"([^\"]*)\" current time delay data object in the response body$")
    public void user_should_see_current_time_delay_data_object_in_the_response_body(int number) throws Throwable {

        jsonAsArrayList = from(base.response.asString()).get("data");

        //System.out.println("jsonAsArrayList  is: " + jsonAsArrayList.toString());

        Assert.assertEquals(number, jsonAsArrayList.size());

    }

    @Then("^different between time delay date value and current time should be within \"([^\"]*)\" seconds$")
    public void different_between_time_delay_date_value_and_current_time_should_be_within_seconds(Long threshold) throws Throwable {

        Date currentDate = new Date();
        Long currentTime = currentDate.getTime() / 1000;  //get the seconds of current time
        System.out.println("Current Date is " + currentDate);

        threshold = threshold * 2;


        for (int i = 0; i < jsonAsArrayList.size(); i++)
        {
            String path = "data.items[" + i + "].date";
            List<Integer> dateList = from(base.response.asString()).getList(path);
            //System.out.println(path);
            //System.out.println(dateList.get(0));

            Long delta =  currentTime - dateList.get(0).longValue();
            System.out.println("\ndelta is "+ delta);
            Assert.assertTrue( delta < threshold);
        }

    }



}
