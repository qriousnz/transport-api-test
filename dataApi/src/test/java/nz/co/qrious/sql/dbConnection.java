package nz.co.qrious.sql;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.*;
import java.util.Arrays;

public class dbConnection {

    //private final String url = "jdbc:postgresql://product-store-test-db01.corp.qrious.co.nz:5432/transport_test_db";
    private final String url = System.getProperty("databaseUrl");
    private final String user = System.getProperty("databaseUser");
    private final String password = System.getProperty("databasePassword");


    public Connection getConnect() throws SQLException {

        return  DriverManager.getConnection(url, user, password);
    }


    public void insertBatch() throws SQLException, IOException {

        Connection con = getConnect();
        con.setAutoCommit(false);
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

        //String currentDir = System.getProperty("user.dir");

        BufferedReader reader = new BufferedReader(new FileReader(  "src/test/resource/testPrepare.sql"));

        String SQL;
        while ((SQL = reader.readLine()) !=null){
            if ( ! SQL.startsWith("--")) {
                 stmt.addBatch(SQL);
            }
        }

        int affectedRows[] = stmt.executeBatch();
        con.commit();
        System.out.println(affectedRows.length + " rows are added.");


//        rs = stmt.executeQuery("SELECT count(1) FROM transport.alert_logs");
//        rs.next();
//        System.out.println("rows after batch INSERT = "+ rs.getInt(1));

    }


    public void deleteBatch() throws SQLException, IOException {

        Connection con = getConnect();
        con.setAutoCommit(false);
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

        BufferedReader reader = new BufferedReader(new FileReader(  "src/test/resource/testCleanup.sql"));

        String SQL;
        while ((SQL = reader.readLine()) !=null){
            stmt.addBatch(SQL);
        }

        int affectedRows[] = stmt.executeBatch();
        con.commit();
        System.out.println(affectedRows.length + " rows are deleted.");


    }

}
