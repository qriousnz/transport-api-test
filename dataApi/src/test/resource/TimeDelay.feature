Feature: Time Delay API

  @dataApi
  Scenario: Time Delay legacy metrics
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "resolution" parameter to "minutes_15"
    And perform the get request
    Then the response code should be 200
    And user should see "4" time delay data object in the response body
    And user should see items object with below pairs on the label "westbound" node
      | 2017-07-01 08:00:00   | 20  |
      | 2017-07-01 08:15:00   | 50  |
      | 2017-07-01 08:30:00   | 70  |


  @dataApi
  Scenario: Time Delay legacy metrics with comparestarttime
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "resolution" parameter to "minutes_15"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" time delay data object in the response body
    And user should see items object with below pairs on the label "westbound" node
      | 2017-07-01 08:00:00   | 20  | 25  |
      | 2017-07-01 08:15:00   | 50  | 55  |
      | 2017-07-01 08:30:00   | 70  | 75  |


  @dataApi
  Scenario: Time Delay legacy metrics with comparestarttime and resolution=days_1
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-04 00:00:00"
    And set the "resolution" parameter to "days_1"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the label "eastbound" node
      | 2017-07-01 00:00:00   | 74   | null  |
      | 2017-07-02 00:00:00   | 114  | null  |


  @dataApi
  Scenario: Time Delay with no data in database during the specific time frame
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-05-01 00:00:00" to "2017-05-02 00:00:00"
    And set the "resolution" parameter to "days_1"
    And perform the get request
    Then the response code should be 200


  @dataApi
  Scenario: Time Delay new metrics with comparestarttime and resolution=minutes_15
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And set the "resolution" parameter to "minutes_15"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" time delay data object in the response body
    And user should see items object with below pairs on the label "westbound" node
      | 2017-07-01 08:00:00   | 20  | 25  |
      | 2017-07-01 08:15:00   | 50  | 55  |
      | 2017-07-01 08:30:00   | 70  | 75  |


  @dataApi
  Scenario: Time Delay new metrics with comparestarttime and resolution=days_1
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/timedelay"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-04 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And set the "resolution" parameter to "days_1"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" time delay data object in the response body
    And user should see items object with below pairs on the label "eastbound" node
      | 2017-07-01 00:00:00   | 74   | null  |
      | 2017-07-02 00:00:00   | 114  | null  |










