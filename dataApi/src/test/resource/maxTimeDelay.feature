Feature: Max Time Delay API

#  @dataApi
#  Scenario: Trigger Dkron job successfully
#    Given the Dkron job base path is "https://transport-dkron-dashboard.test-paas.corp.qrious.co.nz/v1/jobs/"
#    When user configures the Dkron job name to "riccarton_travel_times"
#    And perform the post request to trigger the job
#    Then the response code should be 200


  @dataApi
  Scenario: Max Time Delay legacy metrics
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/maxtimedelay"
    And user specifies the time range from "2017-07-30 00:00:00" to "2017-07-31 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "1" max time delay data object in the response body
    And the value in the max time delay data object is "36"


  @dataApi
  Scenario: Max Time Delay legacy metrics with two max values
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/maxtimedelay"
    And user specifies the time range from "2017-07-28 00:00:00" to "2017-07-29 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "2" max time delay data object in the response body
    And the value in the max time delay data object is "360"


  @dataApi
  Scenario: Max Time Delay with no data in database during the specific time frame
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/maxtimedelay"
    And user specifies the time range from "2017-05-01 00:00:00" to "2017-05-02 00:00:00"
    And perform the get request
    Then the response code should be 200



  @dataApi
  Scenario: Max Time Delay new metrics
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/maxtimedelay"
    And user specifies the time range from "2017-07-30 00:00:00" to "2017-07-31 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And perform the get request
    Then the response code should be 200
    And user should see "1" max time delay data object in the response body
    And the value in the max time delay data object is "36"


  @dataApi
  Scenario: Max Time Delay new metrics with two max values
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/maxtimedelay"
    And user specifies the time range from "2017-07-28 00:00:00" to "2017-07-29 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And perform the get request
    Then the response code should be 200
    And user should see "2" max time delay data object in the response body
    And the value in the max time delay data object is "360"


