Feature: Queue Length API

  @dataApi
  Scenario: Queue Length legacy metrics
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "resolution" parameter to "minutes_15"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the alert id "8" node
      | 2017-07-01 08:00:00   | 20  |
      | 2017-07-01 08:15:00   | 50  |
      | 2017-07-01 08:30:00   | 80  |
      | 2017-07-01 08:45:00   | 110 |
      | 2017-07-01 09:00:00   | 130 |


  @dataApi
  Scenario: Queue Length legacy metrics with comparestarttime
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "resolution" parameter to "minutes_15"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the alert id "8" node
      | 2017-07-01 08:00:00   | 20  | 25  |
      | 2017-07-01 08:15:00   | 50  | 55  |
      | 2017-07-01 08:30:00   | 80  | 85  |
      | 2017-07-01 08:45:00   | 110 | 115 |
      | 2017-07-01 09:00:00   | 130 | 135 |


  @dataApi
  Scenario: Queue Length legacy metrics with comparestarttime and resolution=days_1
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-04 00:00:00"
    And set the "resolution" parameter to "days_1"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the alert id "9" node
      | 2017-07-01 00:00:00   | 130  | null  |
      | 2017-07-02 00:00:00   | 199  | null  |
      | 2017-07-03 00:00:00   | 39   | null  |


  @dataApi
  Scenario: Queue Length with no data in database during the specific time frame
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-05-01 00:00:00" to "2017-05-02 00:00:00"
    And set the "resolution" parameter to "days_1"
    And perform the get request
    Then the response code should be 200


  @dataApi
  Scenario: Queue Length new metrics with comparestarttime and resolution=minutes_15
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-02 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And set the "resolution" parameter to "minutes_15"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the alert id "8" node
      | 2017-07-01 08:00:00   | 20  | 25  |
      | 2017-07-01 08:15:00   | 50  | 55  |
      | 2017-07-01 08:30:00   | 80  | 85  |
      | 2017-07-01 08:45:00   | 110 | 115 |
      | 2017-07-01 09:00:00   | 130 | 135 |


  @dataApi
  Scenario: Queue Length new metrics with comparestarttime and resolution=days_1
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/queuelength"
    And user specifies the time range from "2017-07-01 00:00:00" to "2017-07-04 00:00:00"
    And set the "useNewMetrics" parameter to "true"
    And set the "resolution" parameter to "days_1"
    And set the "comparestarttime" parameter to "2017-06-01 00:00:00"
    And perform the get request
    Then the response code should be 200
    And user should see "4" queue length data object in the response body
    And user should see items object with below pairs on the alert id "9" node
      | 2017-07-01 00:00:00   | 130  | null  |
      | 2017-07-02 00:00:00   | 199  | null  |
      | 2017-07-03 00:00:00   | 39   | null  |






