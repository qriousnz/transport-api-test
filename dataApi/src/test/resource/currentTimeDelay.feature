Feature: Current Time Delay API

  @dataApi
  Scenario: Trigger Dkron job successfully
    Given the Dkron job base path is "https://transport-dkron-dashboard.test-paas.corp.qrious.co.nz/v1/jobs/"
    When user configures the Dkron job name to "riccarton_travel_times"
    And perform the post request to trigger the job
    Then the response code should be 200
    And wait for "20" seconds before test the other scenarios


  @dataApi
  Scenario: Current Time Delay legacy metrics response code verification
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/currenttimedelay"
    And perform the get request
    Then the response code should be 200


  @dataApi
  Scenario: Current Time Delay legacy metrics date value range verification
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/currenttimedelay"
    And perform the get request
    Then the response code should be 200
    And user should see "4" current time delay data object in the response body
    And different between time delay date value and current time should be within "600" seconds


  @dataApi
  Scenario: Current Time Delay new metrics response code verification
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/currenttimedelay"
    And set the "useNewMetrics" parameter to "true"
    And perform the get request
    Then the response code should be 200


  @dataApi
  Scenario: Current Time Delay new metrics date value range verification
    Given the API endpoint base path is "https://transport-api.test-paas.corp.qrious.co.nz:443/api"
    When user setup a request path to "/customers/2/sites/5/currenttimedelay"
    And set the "useNewMetrics" parameter to "true"
    And perform the get request
    Then the response code should be 200
    And user should see "4" current time delay data object in the response body
    And different between time delay date value and current time should be within "600" seconds

