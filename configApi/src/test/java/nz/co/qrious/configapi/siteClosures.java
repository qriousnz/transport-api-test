package nz.co.qrious.configapi;

/**
 * Created by jingbai on 7/28/17.
 */
public class siteClosures {

    public siteClosures(String name, String label, String geom)
    {
        this.name = name;
        this.label = label;
        this.geom = geom;
    }

    private String name;
    private String label;
    private String geom;

    public String getName()
    {
        return name;
    }

    public String getLabel()
    {
        return label;
    }

    public String getGeom()
    {
        return geom;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public void setGeom(String label)
    {
        this.label = geom;
    }

}
