package nz.co.qrious.configapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.DataTableRow;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.Assert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.core.IsEqual.equalTo;

public class trackController {

    String url;
    Response response;
    Map<String, String> postMap = new LinkedHashMap<String,String>();;
    String postBody;
    String trackId;


    @Given("^the track-controller API base path is \"([^\"]*)\"$")
    public void the_track_controller_API_base_path_is(String url) throws Throwable {
        this.url = url;
    }

    @Given("^user set the \"([^\"]*)\" parameter to \"([^\"]*)\"$")
    public void user_set_the_parameter_to(String param, String value) throws Throwable {
        if(url.contains("?") ) {
            url = url + "&" + param + "=" + value;
        }
        else {
            url = url + "?" + param + "=" + value;
        }

        //System.out.println("url is " + url);
    }

    @Given("^with post body contains new track info$")
    public void with_post_body_contains_new_track_info(DataTable trackInfo) throws Throwable {
        //Map<String, Object> hm = new LinkedHashMap<String,Object>();
        for (DataTableRow row : trackInfo.getGherkinRows()) {
            this.postMap.put(row.getCells().get(0), row.getCells().get(1));
        }

        this.postBody =  new ObjectMapper().writeValueAsString(this.postMap);

    }

    @When("^perform the post request to create the new track$")
    public void perform_the_post_request_to_create_the_new_track() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "*/*");

        response = given()
                .headers(headers)
                .accept(ContentType.JSON)
                .body(this.postBody)
                .log().all()
                .when()
                .post(this.url)
                .then()
                .log().all()
                .assertThat().statusCode(201)
                .extract()
                .response();

    }

    @Then("^the response code should be the (\\d+)$")
    public void the_response_code_should_be_the(int code) throws Throwable {
        Assert.assertEquals(code, response.getStatusCode());
    }

    @Then("^the new track id is return in the \"([^\"]*)\" parameter in the header$")
    public void the_new_track_id_is_return_in_the_parameter_in_the_header(String header) throws Throwable {
        String locationValue = response.getHeader(header);
        Pattern pattern = Pattern.compile("tracks/(\\d+)?");
        Matcher matcher = pattern.matcher(locationValue);

        Assert.assertTrue(matcher.find());

        trackId = matcher.group(1);
    }

    @Then("^save the trackid into \"([^\"]*)\" file for further reference$")
    public void save_the_trackid_file_into_for_further_reference(String fileName) throws Throwable {
        FileWriter writer = new FileWriter(fileName,false);
        writer.write(trackId);
        writer.close();
    }

    @Given("^user gets the id from \"([^\"]*)\" file and attach it to the request path$")
    public void user_gets_the_id_from_file_and_attach_it_to_the_request_path(String fileName) throws Throwable {
        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferReader = new BufferedReader(fileReader);
        trackId = bufferReader.readLine();
        //System.out.println("read the trackId from file is: " + trackId);
        url = url + "/" + trackId;
    }

    @Given("^user performs the get request$")
    public void user_performs_the_get_request() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();
        response = given()
                .accept(ContentType.JSON)
                .log().all()    // .log().all()
                .when()
                .get(this.url)
                .then()
                .log().all()    // .log().all()   ifValidationFails
                .extract()
                .response();
    }

    @Then("^All the \"([^\"]*)\" key are with value equals to \"([^\"]*)\"$")
    public void All_the_key_are_with_value_equals_to(String key, String value) throws Throwable {
        String responseString = response.asString();

        String regX = "(?i)\"" + key + "\":(\\d+)";
        Pattern pattern = Pattern.compile(regX);                   //"(?i)\"SITEID\":(\\d+)"
        Matcher matcher = pattern.matcher(responseString);

        while (matcher.find()) {
            //System.out.println( "Matcher group 0 for " + key +  "is :" +  matcher.group(0));
            Assert.assertEquals(matcher.group(1), value);
        }
    }



    @Then("^The JSON response key \"([^\"]*)\" value equals to \"([^\"]*)\"$")
    public void The_JSON_response_key_value_equals_to(String key, String value) throws Throwable {
        JsonPath json = new JsonPath(response.asString());

        if (value.equalsIgnoreCase("trackId")) {
            FileReader fileReader = new FileReader("trackId");
            BufferedReader bufferReader = new BufferedReader(fileReader);
            trackId = bufferReader.readLine();
            Assert.assertTrue(json.getString(key).contains(trackId));
        }
        else {
            Assert.assertTrue(json.getString(key).contains(value));
        }

    }

    @Given("^user performs the delete request$")
    public void user_performs_the_delete_request() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();
        response = given()
                .accept(ContentType.JSON)
                .log().all()    // .log().all()   .log().ifValidationFails()
                .when()
                .delete(this.url)
                .then()
                .log().all()    // .log().all()
                .extract()
                .response();

    }


    @Then("^The JSON response key \"([^\"]*)\" doesn't exist$")
    public void The_JSON_response_key_doesn_t_exist(String key) throws Throwable {
        JsonPath json = new JsonPath(response.asString());

        Assert.assertEquals("[null]", json.getString(key));

    }



}
