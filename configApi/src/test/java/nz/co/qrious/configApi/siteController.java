package nz.co.qrious.configapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.DataTableRow;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * Created by jingbai on 7/28/17.
 */
public class siteController {

    String url;
    Response response;
    Map<String, Object> postMap = new LinkedHashMap<String,Object>();;
    String postBody;
    String lastSiteId;


    @Given("^the site-controller API base path is \"([^\"]*)\"$")
    public void the_site_controller_API_base_path_is(String url) throws Throwable {
        this.url = url;
    }

    @When("^a user setup a request path to \"([^\"]*)\"$")
    public void a_user_setup_a_request_path_to(String path) throws Throwable {
        this.url = this.url + path;
        //System.out.println("URL is " + this.url);
    }

    @When("^perform the get request$")
    public void perform_the_get_request() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();
        response = given()
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .get(this.url)
                .then()
                .log().all()
                .extract()
                .response();
    }

    @Then("^the response code should be (\\d+)$")
    public void the_response_code_should_be(int code) throws Throwable {
        Assert.assertEquals(code, response.getStatusCode());
        //System.out.println("status code is " + response.getStatusCode());

    }

    @Then("^I should see JSON response object with pairs$")
    public void I_should_see_JSON_response_object_with_pairs(DataTable dataTable) throws Throwable {

        JsonPath json = new JsonPath(response.asString());

        for (DataTableRow row : dataTable.getGherkinRows()) {
            //System.out.println(json.getString(row.getCells().get(0)));
            Assert.assertTrue(json.getString(row.getCells().get(0)).contains(row.getCells().get(1)));
        }
    }

    @Then("^I should see JSON response key with \"([^\"]*)\" and value \"([^\"]*)\"$")
    public void I_should_see_JSON_response_key_with_and_value(String key, String value) throws Throwable {
        JsonPath json = new JsonPath(response.asString());
        Assert.assertTrue(json.getString(key).contains(value));

    }


    @When("^with post body contains site info$")
    public void with_post_body_contains_site_info(DataTable siteInfo) throws Throwable {
        //Map<String, Object> hm = new LinkedHashMap<String,Object>();
        for (DataTableRow row : siteInfo.getGherkinRows()) {
            this.postMap.put(row.getCells().get(0), row.getCells().get(1));
        }

        //System.out.println(this.postMap);

    }

    @When("^with post body contains closures object info$")
    public void with_post_body_contains_closures_object_info(List<siteClosures> closureInfo) throws Throwable {

        this.postMap.put("closures", closureInfo);

        this.postBody =  new ObjectMapper().writeValueAsString(this.postMap);
    }


    @When("^perform the post request$")
    public void perform_the_post_request() throws Throwable {
        RestAssured.useRelaxedHTTPSValidation();

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");

        System.out.println("Debug: post the request to create a site");

        response = given()
                .headers(headers)
                .accept(ContentType.JSON)
                .body(this.postBody)
                .log().all()
                .when()
                .post(this.url)
                .then()
                .log().all()
                .extract()
                .response();

        System.out.println("Debug: after post the request to create a site");
        System.out.println(response.asString());

    }

    @Then("^get the last siteId in the response$")
    public void get_the_last_siteId_in_the_response() throws Throwable {
        JsonPath json = new JsonPath(response.asString());

        String jsonString = json.get("siteId").toString();
        jsonString = jsonString.substring(1,jsonString.length()-1);

        //System.out.println(json.get("siteId"));
        //System.out.println(jsonString);


        String[] numberSplit = jsonString.split(",") ;
        lastSiteId = numberSplit[ (numberSplit.length-1) ].trim();

        System.out.println(lastSiteId);

    }

    @Then("^a user setup the request with the siteId attached to the path$")
    public void a_user_setup_the_request_with_the_siteId_attached_to_the_path() throws Throwable {

        this.url = this.url + "/" + lastSiteId;
        //System.out.println("URL is " + this.url);

    }


    @When("^perform the delete request$")
    public void perform_the_delete_request() throws Throwable {

        RestAssured.useRelaxedHTTPSValidation();
        response = given()
                .accept(ContentType.JSON)
                .when()
                .delete(this.url);

    }
}
