package nz.co.qrious.configapi;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by jingbai on 7/28/17.
 */
@RunWith(Cucumber.class)
@Cucumber.Options (
        format ={"json:target/cucumber.json","json:../target/cucumber_configApi.json"},
        features = "src/test/resource",
        glue = { "nz.co.qrious.configapi"},
        tags = { "@configApi"}
)
public class TestRunner {
}

