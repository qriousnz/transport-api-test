Feature: Track controller API


  @configApi
  Scenario: 1 create a new track of a site
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user set the "queueMonitoring" parameter to "true"
    And user set the "delayMonitoring" parameter to "true"
    And with post body contains new track info
      | name   	    | Grey Lynn for site 19					|
      | wkt	        | LINESTRING(174.74657000000002 -36.865370000000006, 174.74628 -36.86478, 174.74579000000003 -36.864940000000004, 174.74507000000003 -36.86518, 174.74452000000002 -36.865390000000005, 174.74477000000002 -36.865880000000004, 174.74521000000001 -36.8667)	|
    When perform the post request to create the new track
    Then the response code should be the 201
    And the new track id is return in the "location" parameter in the header
    And save the trackid into "trackId" file for further reference


  @configApi
  Scenario: 2 get the new track info with the trackId and siteid
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user gets the id from "trackId" file and attach it to the request path
    And user performs the get request
    Then the response code should be the 200
    And All the "siteId" key are with value equals to "19"
    And The JSON response key "data.id" value equals to "trackId"
    And The JSON response key "data.queueMonitoring.definition.alertType" value equals to "QUEUE"
    And The JSON response key "data.travelTimeMonitoring.definition.alertType" value equals to "TRAVEL_TIME"


  @configApi
  Scenario: 3 get all the tracks info with siteid
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user performs the get request
    Then the response code should be the 200
    And All the "siteId" key are with value equals to "19"
    And The JSON response key "data.id" value equals to "trackId"
    And The JSON response key "data.queueMonitoring.definition.alertType" value equals to "QUEUE"
    And The JSON response key "data.travelTimeMonitoring.definition.alertType" value equals to "TRAVEL_TIME"


  @configApi
  Scenario: 4 get the new track info with the correct trackId but wrong siteid
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/5/tracks"
    And user gets the id from "trackId" file and attach it to the request path
    And user performs the get request
    Then the response code should be the 400


  @configApi
  Scenario: 5 delete the new track info within the site
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user gets the id from "trackId" file and attach it to the request path
    And user performs the delete request
    Then the response code should be the 204



  @configApi
  Scenario: 6 create a new track of a site with only queueMonitoring set to true
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user set the "queueMonitoring" parameter to "true"
    And user set the "delayMonitoring" parameter to "false"
    And with post body contains new track info
      | name   	    | Grey Lynn for site 19					|
      | wkt	        | LINESTRING(174.74657000000002 -36.865370000000006, 174.74628 -36.86478, 174.74579000000003 -36.864940000000004, 174.74507000000003 -36.86518, 174.74452000000002 -36.865390000000005, 174.74477000000002 -36.865880000000004, 174.74521000000001 -36.8667)	|
    When perform the post request to create the new track
    Then the response code should be the 201
    And the new track id is return in the "location" parameter in the header
    And save the trackid into "trackId" file for further reference


  @configApi
  Scenario: 7 get all the tracks info with siteid which only deplayMonitoring is set to true
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user performs the get request
    Then the response code should be the 200
    And All the "siteId" key are with value equals to "19"
    And The JSON response key "data.id" value equals to "trackId"
    And The JSON response key "data.queueMonitoring.definition.alertType" value equals to "QUEUE"
    And The JSON response key "data.travelTimeMonitoring" doesn't exist


  @configApi
  Scenario: 8 try to delete the new track but use a wrong site id
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/5/tracks"
    And user gets the id from "trackId" file and attach it to the request path
    And user performs the delete request
    Then the response code should be the 400


  @configApi
  Scenario: 9 delete the new track info within the site
    Given the track-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver/sites/19/tracks"
    And user gets the id from "trackId" file and attach it to the request path
    And user performs the delete request
    Then the response code should be the 204
