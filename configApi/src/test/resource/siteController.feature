Feature: Site controller API

  @configApi
  Scenario: Get sites with customer id
    Given the site-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver"
    When a user setup a request path to "/customers/2/sites"
    And perform the get request
    Then the response code should be 200
    And I should see JSON response object with pairs
      | siteId      | 5                                            |
      | siteName    | Riccarton/Deans Ave                          |
      | siteAddress | Riccarton rd, Riccarton, Christchurch 8011   |


  @configApi
  Scenario: Get site detail with customer id and site id
    Given the site-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver"
    When a user setup a request path to "/customers/2/sites/5"
    And perform the get request
    Then the response code should be 200
    And I should see JSON response key with "siteId" and value "5"



  @configApi
  Scenario: Create a new site
    Given the site-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver"
    When a user setup a request path to "/customers/2/sites"
    And with post body contains site info
      | siteName   	    | testA						|
      | siteAddress	    | 22 test A road, Auckland	|
    And with post body contains closures object info
      | name   	            |  label        | geom                        |
      | testA_closurename	| testA_label	| LINESTRING(172.61223 -43.534310000000005, 172.61222 -43.53036, 172.61224 -43.5289, 172.61225000000002 -43.52879, 172.61233000000001 -43.528800000000004) |
    And perform the post request
    Then the response code should be 201



  Scenario: Delete an exist site
    Given the site-controller API base path is "https://transport-configserver.test-paas.corp.qrious.co.nz:443/configserver"
    When a user setup a request path to "/customers/2/sites"
    And perform the get request
    Then the response code should be 200
    And get the last siteId in the response
    Then a user setup the request with the siteId attached to the path
    And perform the delete request
    Then the response code should be 204

